from random import *
class Input:

    # Methode zum Einlesen oder Generieren des Offsets
    def getoffset(self):

        invalid = 1
        print('Offset der Codierung eingeben(Enter -> Zufall):')

        while invalid > 0:
            invalid = 0
            offset = raw_input()

            # Kontrolle der Nutzereingabe
            if offset:
                for char in offset:
                    if char.isdigit():
                        continue
                    else:
                        invalid += 1
                        print('Nur Zahlen erlaubt!\nBitte erneut eingeben( Enter -> Zufall-Offset):')
                        break
            # Zufall-Offset falls nichts eingegeben wurde
            else:
                offset = randint(0, 1024)

        return int(offset)

    # Methode zum Einlesen der zu (de)codierenden Nachricht
    def getnachricht(self):
        invalid = 1
        print('Zu codierende Nachricht eingeben:')

        while invalid > 0:
            invalid = 0
            nachricht = raw_input()

            # Kontrolle der Nutzereingabe
            if nachricht:
                for char in nachricht:
                    if char.isalpha() or char == ' ' or char.isdigit():
                        continue
                    else:
                        invalid += 1
                        print('Keine Sonderzeichen erlaubt!\nBitte erneut eingeben:')
                        break
            else:
                invalid += 1
                print ('Es wurde keine Nachricht eingegeben!\nBitte erneut eingeben:')

        return nachricht