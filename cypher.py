class Encryption:

    # Methode zum Codieren der 'message' mit einem bestimmten 'offset'
    def encrypt(self, message, offset):
        nachricht = message
        codiert = ''
        caesar = offset

        for buchstabe in nachricht:
            if buchstabe == ' ':
                codiert += buchstabe

            elif buchstabe.isdigit():
                intvalue = ord(buchstabe)
                intvalue -= 48
                intvalue += caesar
                intvalue %= 10
                intvalue += 48
                codiert += chr(intvalue)

            elif buchstabe.islower():
                intvalue = ord(buchstabe)
                intvalue -= 97
                intvalue += caesar
                intvalue %= 26
                intvalue += 97
                codiert += chr(intvalue)

            elif buchstabe.isupper():
                intvalue = ord(buchstabe)
                intvalue -= 65
                intvalue += caesar
                intvalue %= 26
                intvalue += 65
                codiert += chr(intvalue)

        return codiert

    def decrypt(self, message, offset):
        nachricht = message
        decodiert = ''
        caesar = offset

        for buchstabe in nachricht:
            if buchstabe == ' ':
                decodiert += buchstabe

            elif buchstabe.isdigit():
                intvalue = ord(buchstabe)
                intvalue -= 48
                intvalue -= caesar
                intvalue %= 10
                intvalue += 48
                decodiert += chr(intvalue)

            elif buchstabe.islower():
                intvalue = ord(buchstabe)
                intvalue -= 97
                intvalue -= caesar
                intvalue %= 26
                intvalue += 97
                decodiert += chr(intvalue)

            elif buchstabe.isupper():
                intvalue = ord(buchstabe)
                intvalue -= 65
                intvalue -= caesar
                intvalue %= 26
                intvalue += 65
                decodiert += chr(intvalue)

        return decodiert